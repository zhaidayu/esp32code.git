#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "led.h"
#include "driver/timer.h"

#include <stdlib.h>
#include <string.h>
#include "driver/ledc.h"
#include "my_pwm.h"

#define LEDC_MAX_DUTY   (8191)
#define LEDC_FADE_TIME  (2000)

void app_main(void)
{
    my_ledc_pwm_init();
    printf("PWM初始化成功\n");

    while (1)
    {
        //渐变开始
        ledc_set_fade_with_time(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0,LEDC_MAX_DUTY, LEDC_FADE_TIME);
        ledc_fade_start(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, LEDC_FADE_NO_WAIT);
        vTaskDelay(LEDC_FADE_TIME / portTICK_PERIOD_MS);

        uint32_t m=ledc_get_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0);
        printf("占空比为：%u\n",m);

        ledc_set_fade_with_time(LEDC_HIGH_SPEED_MODE,LEDC_CHANNEL_0,0, LEDC_FADE_TIME);
        ledc_fade_start(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, LEDC_FADE_NO_WAIT);
        vTaskDelay(LEDC_FADE_TIME / portTICK_PERIOD_MS);

        uint32_t u=ledc_get_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0);
        printf("占空比为：%u\n",u);
    }   
}
