#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"

#define shanshuo_led   0
#define liushui_led 1
#define led_R_io    GPIO_NUM_2
#define led_G_io    GPIO_NUM_18
#define led_B_io    GPIO_NUM_19

#define led_io_mode GPIO_MODE_OUTPUT

void app_main()
{
#if shanshuo_led            //闪烁
    gpio_reset_pin(led_R_io);
    gpio_set_direction(led_R_io,led_io_mode);
    while (1)
    {
        gpio_set_level(led_R_io,1);
        printf("LED_R_BLINK\n");
        vTaskDelay(1000);
        gpio_set_level(led_R_io,0);
        printf("LED_R_NO_BLINK\n");
        vTaskDelay(1000);
    }
#endif
#if liushui_led            //流水效果
    gpio_reset_pin(led_R_io|led_G_io|led_B_io);
    gpio_set_direction(led_R_io|led_G_io|led_B_io,led_io_mode);
    while (1)
    {
        gpio_set_level(led_R_io,1);
        printf("R灯亮\n");
        gpio_set_level(led_G_io,0);
        printf("G灯亮\n");
        gpio_set_level(led_B_io,0);
        printf("B灯亮\n");
        vTaskDelay(100);
        gpio_set_level(led_R_io,0);
        gpio_set_level(led_G_io,1);
        gpio_set_level(led_B_io,0);
        vTaskDelay(100);
        gpio_set_level(led_R_io,0);
        gpio_set_level(led_G_io,0);
        gpio_set_level(led_B_io,1);
        vTaskDelay(100);
    }
#endif

    
    
}