#ifndef __MYSPI_H_
#define __MYSPI_H_
#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "driver/gpio.h"
#include "driver/spi_master.h"
// SPI_DMA_CH_AUTO
#define spi_num SPI2_HOST
#define spi_dma_channel SPI_DMA_CH_AUTO
#define PIN_NUM_MOSI1    GPIO_NUM_4
#define PIN_NUM_CLK1    GPIO_NUM_5

#define maxSPIFrameInBytes     4096

#define led_num  500
#define led_num_bit    ((led_num + 2)*4*8)
// int spi_write(uint8_t *data, uint8_t len);
int my_spi1_init(void);
int spi_write(void);
void my_ledbit_init(void);
void my_led_index(uint8_t index,uint8_t white,uint8_t red,uint8_t green,uint8_t blue);
void my_led_open(const int ledsize);
void my_led_bit_int(void);

#endif
