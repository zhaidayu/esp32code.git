#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "led.h"
#include "my_spi.h"

void app_main(void)
{
    my_spi1_init();
    my_led_bit_int();
    vTaskDelay(10);
    my_led_open(500);
    printf("%d\n",sizeof(int));

    while(1)
    {
        vTaskDelay(100);
    }

}

