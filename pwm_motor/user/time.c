#include "time.h"

T1 T_1;
extern u32 TIM2CH2_CAPTURE_STA;
u32 hall_value;
void TIM1_init(void)		//时基为1ms
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE); //时钟使能
	//72 000 000/72/1000=1000hz=1ms
	//定时器TIM1初始化
	TIM_TimeBaseStructure.TIM_Period = 1000-1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler =72; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
  TIM_TimeBaseStructure.TIM_RepetitionCounter=0;//不重复计数
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM1,TIM_IT_Update,ENABLE ); //使能指定的TIM3中断,允许更新中断

	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_IRQn;  //TIM3中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;  //先占优先级0级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;  //从优先级3级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器
  


	TIM_Cmd(TIM1, ENABLE);  //使能TIMx
}

void TIM1_UP_IRQHandler(void)
{
	if ( TIM_GetITStatus(TIM1 , TIM_IT_Update) != RESET ) 
	{
    T_1.led_time++;
    T_1.hall_time++;
    if(T_1.led_time>=500)
    {
      LED_Tigge();
      T_1.led_time=0;
    }
    if(T_1.hall_time>=100)
    {
//      if(TIM2CH2_CAPTURE_STA-hall_value>150)
//      {
//        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, DISABLE);
//      }
//      else
//        hall_value=TIM2CH2_CAPTURE_STA;
      T_1.hall_time=0;
    }
		TIM_ClearITPendingBit(TIM1, TIM_IT_Update  );  //清除TIMx更新中断标志
	}
}






//void TIM2_motor_Init(void)
//{
//	TIM_TimeBaseInitTypeDef	TIM_TimeBaseStruct;
//	NVIC_InitTypeDef NVIC_InitStruction;
//	
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,ENABLE);
//	//72000 000Hz/72/5=200 000Hz==200kHz=5us
//	TIM_TimeBaseStruct.TIM_Period = 5-1;
//	TIM_TimeBaseStruct.TIM_Prescaler = 72;
//	TIM_TimeBaseStruct.TIM_ClockDivision = TIM_CKD_DIV1;
//	TIM_TimeBaseStruct.TIM_CounterMode = TIM_CounterMode_Up;
//	TIM_TimeBaseStruct.TIM_RepetitionCounter = 0;
//	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseStruct);
//	
////	TIM_ClearFlag(TIM2, TIM_FLAG_Update);							    		/* 清除溢出中断标志 */
//	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
//																			/* 开启时钟 */
//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2,DISABLE);
//  NVIC_InitStruction.NVIC_IRQChannel=TIM2_IRQn;
//	NVIC_InitStruction.NVIC_IRQChannelPreemptionPriority=2;
//	NVIC_InitStruction.NVIC_IRQChannelSubPriority=0;
//	NVIC_InitStruction.NVIC_IRQChannelCmd=ENABLE;
//	NVIC_Init(&NVIC_InitStruction);
//  TIM_Cmd(TIM2, ENABLE);
//}

//void TIM2_IRQHandler(void)
//{
//	if(	TIM_GetITStatus(TIM2,TIM_IT_Update) != RESET)
//	{

//		TIM_ClearITPendingBit(TIM2, TIM_FLAG_Update);
//	}
//}






