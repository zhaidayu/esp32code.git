#include "led.h"




void Init_LedLamp(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_APB2PeriphClockCmd(LED_APB_GPIO, ENABLE);	 //使能PB,PE端口时钟
	GPIO_InitStructure.GPIO_Pin =LED_GPIO_PIN;			 //LED0-->PC.13 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(LED_GPIO, &GPIO_InitStructure);					 //根据设定参数初始化GPIOC.13
//	GPIO_SetBits(LED_GPIO,LED_GPIO_PIN);
}

