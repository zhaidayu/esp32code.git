#include "pwm.h"

u32 TIM2CH2_CAPTURE_STA=0; //输入捕获状态

/**
  * @brief  配置TIM2输出的PWM信号的模式，如周期、极性、占空比
  * @param  无
  * @retval 无
  */
/*
 * TIMxCLK/CK_PSC --> TIMxCNT --> TIMx_ARR --> TIMxCNT 重新计数
 *                    TIMx_CCR(电平发生变化)
 * 信号周期=(TIMx_ARR +1 ) * 时钟周期
 * 占空比=TIMx_CCR/(TIMx_ARR +1)
 */
static void TIM2_Mode_Config(u16 arr,u16 psc)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
  TIM_ICInitTypeDef  TIM_ICInitStructure;
	NVIC_InitTypeDef  NVIC_InitStruction;
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); //使能定时器3的时钟
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO , ENABLE); 
  
  //pwm引脚
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;		    // 复用推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  //霍尔
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;		    // 输入
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  //方向引脚
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		    // 推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_SetBits(GPIOA,GPIO_Pin_0);


/* ----------------------------------------------------------------------- 
    TIM2 Channel1 duty cycle = (TIM2_CCR1/ TIM2_ARR+1)* 100% = 50%
    TIM2 Channel2 duty cycle = (TIM2_CCR2/ TIM2_ARR+1)* 100% = 37.5%
    TIM2 Channel3 duty cycle = (TIM2_CCR3/ TIM2_ARR+1)* 100% = 25%
    TIM2 Channel4 duty cycle = (TIM2_CCR4/ TIM2_ARR+1)* 100% = 12.5%
  ----------------------------------------------------------------------- */
  /* Time base configuration */		 
  TIM_TimeBaseStructure.TIM_Period = arr-1;       //自动重装载值
  TIM_TimeBaseStructure.TIM_Prescaler = psc-1;	    //设置预分频值
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;//设置时钟分隔	
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //向上计数模式
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
  
	TIM_ITConfig(TIM2,TIM_IT_CC1,ENABLE);   //TIM2中断使能
  /* PWM1 Mode configuration: Channel1 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;	    //配置为PWM模式1
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;	
  TIM_OCInitStructure.TIM_Pulse =arr>>1;	   //设置跳变值，当计数器计数到这个值时，电平发生跳变
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  TIM_OC3Init(TIM2, &TIM_OCInitStructure);	 //使能通道1
  
  TIM_ICInitStructure.TIM_Channel=TIM_Channel_2;
  TIM_ICInitStructure.TIM_ICFilter=0X00;
  TIM_ICInitStructure.TIM_ICPolarity=TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICPrescaler=TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICSelection=TIM_ICSelection_DirectTI;
  TIM_ICInit(TIM2, &TIM_ICInitStructure);
  
  TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);//启用预加载寄存器CCR

  TIM_ARRPreloadConfig(TIM2, ENABLE);			 // 使能TIM2重载寄存器ARR
	
	NVIC_InitStruction.NVIC_IRQChannel=TIM2_IRQn;
	NVIC_InitStruction.NVIC_IRQChannelPreemptionPriority=3;
	NVIC_InitStruction.NVIC_IRQChannelSubPriority=0;
	NVIC_InitStruction.NVIC_IRQChannelCmd=ENABLE;
	NVIC_Init(&NVIC_InitStruction);
  
	TIM_ITConfig(TIM2,TIM_IT_CC2,ENABLE); 
  TIM_Cmd(TIM2, ENABLE);                   //使能定时器3	
//	TIM_DMACmd(TIM2,TIM_DMA_Update,ENABLE);
//  TIM_SetCompare3(TIM2,180);
  
//  TIM_ITConfig(TIM2,TIM_IT);
}

void TIM2_IRQHandler()
{

  if (TIM_GetITStatus(TIM2, TIM_IT_CC2) != RESET)//捕获 1 发生捕获事件
  {
    TIM2CH2_CAPTURE_STA++;
    TIM_ClearITPendingBit(TIM2,TIM_IT_CC2);
  }
//	if(TIM_GetFlagStatus(TIM2,TIM_IT_Update)!=RESET)
//	{

//		TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
//	}
  TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
}

void TIM2_PWM_Init(void)
{
	TIM2_Mode_Config(10,0);	
}
