#ifndef __LED_H_
#define __LED_H_

#include "main.h"

#define LED_APB_GPIO  RCC_APB2Periph_GPIOC

#define LED_GPIO  GPIOC

#define LED_GPIO_PIN  GPIO_Pin_13

#define LED_Tigge() GPIOC->ODR^=LED_GPIO_PIN

void Init_LedLamp(void);

#endif
