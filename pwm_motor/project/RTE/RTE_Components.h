
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'pwm_motor' 
 * Target:  'pwm_motor' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define RTE_DEVICE_STDPERIPH_CAN
#define RTE_DEVICE_STDPERIPH_DMA
#define RTE_DEVICE_STDPERIPH_FLASH
#define RTE_DEVICE_STDPERIPH_FRAMEWORK
#define RTE_DEVICE_STDPERIPH_GPIO
#define RTE_DEVICE_STDPERIPH_RCC
#define RTE_DEVICE_STDPERIPH_TIM

#endif /* RTE_COMPONENTS_H */
