#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "esp_log.h"
#include "led.h"
#include "my_can.h"

uint8_t can_buff1[8]={11,22,33,44,55,66,77,88};
void app_main(void)
{
    my_can_init();
    printf("%d\n",sizeof(int));
    CAN_Send_Task(0x200,8, can_buff1);

}

