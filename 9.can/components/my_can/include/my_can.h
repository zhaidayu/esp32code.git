#ifndef __MYCAN_H_
#define __MYCAN_H_


#include <stdio.h>
#include "driver/twai.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"

#define can_tx_io    GPIO_NUM_4
#define can_rx_io    GPIO_NUM_16
#define can_mode    TWAI_MODE_NORMAL
#define can_baud    TWAI_TIMING_CONFIG_500KBITS() 
uint8_t can_buff[8];


void my_can_init(void);
void CAN_Receive_Task(void *arg);
void CAN_Send_Task(uint32_t can_id,uint8_t can_len,uint8_t *msg);


#endif
