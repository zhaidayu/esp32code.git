#include <stdio.h>
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "led.h"




void led_init(void)
{
    // gpio_config_t gpio_config_structure;

    // gpio_config_structure.pin_bit_mask = (1ULL << led_gpio);
    // gpio_config_structure.intr_type = 0;
    // gpio_config_structure.mode = GPIO_MODE_OUTPUT;
    // gpio_config_structure.pull_down_en = 0;
    // gpio_config_structure.pull_up_en = 0;
    // gpio_config(&gpio_config_structure);
    // gpio_set_level(led_gpio,0);
    gpio_pad_select_gpio(led_gpio);
    gpio_set_direction(led_gpio, GPIO_MODE_OUTPUT);
    gpio_set_level(led_gpio, 0);

}
void setLED(int i)
{
    gpio_set_level(led_gpio, i);
}
