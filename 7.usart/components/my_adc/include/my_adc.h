#ifndef __MYADC_H_
#define __MYADC_H_

#include "driver/adc.h"
#include "esp_adc_cal.h"
//定义位宽，2^10=1024
#define my_adc1_width  ADC_WIDTH_BIT_10
//定义通道
#define my_adc1_channel    ADC1_CHANNEL_0
//定义adc衰减
#define my_adc1_atten    ADC_ATTEN_DB_0
//参考电压
#define DEFAULT_VREF    1100 


void my_adc1_init(void);
int my_adc1_get_value(int m);
int my_adc1_get_vol_mv(int m);

#endif
