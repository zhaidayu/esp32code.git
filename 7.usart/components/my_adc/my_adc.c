#include <stdio.h>
#include "my_adc.h"


static esp_adc_cal_characteristics_t *adc_chars;

void my_adc1_init(void)
{
    adc1_config_width(my_adc1_width);//配置位宽
    adc1_config_channel_atten(my_adc1_channel, my_adc1_atten);//配置通道衰减
}

int value=0;
int my_adc1_get_value(int m)
{
    
    for ( int i = 0; i < m; i++)
    {
        value+=adc1_get_raw(my_adc1_channel);//采集电压值
        /* code */
    }
    value = value / m;
    return value;
}
int my_adc1_get_vol_mv(int m)
{
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));//分配一个内存块
    esp_adc_cal_characterize(ADC_UNIT_1, my_adc1_atten, my_adc1_width, DEFAULT_VREF, adc_chars);
    int va=my_adc1_get_value(m);
    int voltage = esp_adc_cal_raw_to_voltage(va, adc_chars);
    return voltage;
} 