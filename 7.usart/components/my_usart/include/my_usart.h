#ifndef __MYUSART_H_
#define __MYUSART_H_
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "driver/uart.h"
#include "string.h"
#include "driver/gpio.h"

#define my_Tx_PIN   (GPIO_NUM_4)
#define my_Rx_PIN     (GPIO_NUM_5)
#define my_uart_baud    (115200)
#define my_uart_data_size   (UART_DATA_8_BITS)
#define my_uart_set_parity    (UART_PARITY_DISABLE)
#define my_uart_set_stop_bit  (UART_STOP_BITS_1)

void my_usart_init(void);
int my_usart_sendData(const char* logName, const char* data);

void run_usart_task(void);

#endif
