#ifndef __MYADC_H_
#define __MYADC_H_

#include <stdio.h>
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "esp_log.h"

// #include "driver/i2s.h"
static const char *TAG = "ADC DMA";
#define TIMES              256
#define GET_UNIT(x)        ((x>>3) & 0x1)//看使用的是ADC1还是ADC2，第四位为0是ADC1，为1是ADC2

#if CONFIG_IDF_TARGET_ESP32
#define ADC_RESULT_BYTE     2
#define ADC_CONV_LIMIT_EN   1                       //For ESP32, this should always be set to 1，因为ADC2不支持DMA
#define ADC_CONV_MODE       ADC_CONV_SINGLE_UNIT_1  //ESP32 only supports ADC1 DMA mode
#define ADC_OUTPUT_TYPE     ADC_DIGI_OUTPUT_FORMAT_TYPE1

#elif CONFIG_IDF_TARGET_ESP32S2
#define ADC_RESULT_BYTE     2
#define ADC_CONV_LIMIT_EN   0
#define ADC_CONV_MODE       ADC_CONV_SINGLE_UNIT_1//只转换ADC1
#define ADC_OUTPUT_TYPE     ADC_DIGI_OUTPUT_FORMAT_TYPE1
#endif


bool check_valid_data(const adc_digi_output_data_t *data);//判断数据是不是有效的；
void my_adc_dma_init(void);



#endif
