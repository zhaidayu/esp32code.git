#ifndef __TIMER_H_
#define __TIMER_H_



#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "driver/timer.h"
#include "led.h"

void my_timer_init(void);

void  print_timer_counter(uint64_t counter_value);

#endif
