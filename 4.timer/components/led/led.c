#include "led.h"

void led_init(void)
{
    gpio_config_t gpio_config_structure;

    gpio_config_structure.pin_bit_mask = (1ULL << led_gpio);
    gpio_config_structure.intr_type = 0;
    gpio_config_structure.mode = GPIO_MODE_OUTPUT;
    gpio_config_structure.pull_down_en = 0;
    gpio_config_structure.pull_up_en = 0;
    gpio_config(&gpio_config_structure);
    gpio_set_level(led_gpio,0);

}

