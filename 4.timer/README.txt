esp32定时器
两组，每组两个,共四个定时器

操作步骤：

1、定时器初始化
timer_init(TIMER_GROUP_x,TIMER_x,&timer_config_t;)
参数1、定时器组0-1；2、定时器号0-1；3、结构体指针
typedef struct {
    timer_alarm_t alarm_en;      /*!< 报警使能 */
    timer_start_t counter_en;    /*!< 计数使能，初始化时通常设置为PAUSE，在开启定时器的时候计数器就会开始计数 */
    timer_intr_mode_t intr_type; /*!< 中断模式 */计数器报警后是否产生中断
    timer_count_dir_t counter_dir; /*!<计数方向  */	//向上或者向下计数
    timer_autoreload_t auto_reload;   /*!< 自动重装载*/计数器报警后是否自动重载指定的值
    uint32_t divider;   /*!< 预分频值.  2 to 65536. */
} timer_config_t;

2、设置计数器的计数开始值	num:值
timer_set_counter_value(TIMER_GROUP_x, TIMER_x, num);

3、设置报警值
timer_set_alarm_value(TIMER_GROUP_x, TIMER_x, num);
num：计数多少次进入中断
1/(Base_clock/divider/num)=定时器时间

4、使能定时器中断
timer_enable_intr(TIMER_GROUP_x, TIMER_x);

5、添加中断回调函数
timer_isr_callback_add();

6、打开定时器
timer_start();

7、配置定时器中断回调函数
在中断回调函数里不能进行打印功能


当选择系统时钟为80mhz、预分频值设置为80时，80 000 000/80 = 1 000 000;每计数一次是1us，此时设置报警值为1，程序会一直重启（频繁进入中断）
